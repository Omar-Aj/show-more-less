import React from "react";

import TextLimiter from "./components/TextLimiter";
import TextExamples from "./components/TextExample";

function App() {
  return (
    <div className="App">
      <TextLimiter limit={1}>{TextExamples[2]}</TextLimiter>
      <TextLimiter>{TextExamples[0]}</TextLimiter>
    </div>
  );
}

export default App;
