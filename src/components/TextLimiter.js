import React, { useState } from "react";

import "./Text.css";

function TextLimiter(props) {
  const [readMore, setReadMore] = useState(false);
  const linkName = readMore ? "Read Less << " : "Read More >> ";

  function handleShow() {
    let theText = document.querySelector("#textInfo");
    theText.classList.remove("limited-text");
  }
  function handleHide() {
    let theText = document.querySelector("#textInfo");
    theText.classList.add("limited-text");
  }

  if (props.limit) {
    return (
      <div className="text">
        <p
          id="textInfo"
          className="limited-text"
          style={{ WebkitLineClamp: props.limit, lineClamp: props.limit }}
        >
          {props.children}
        </p>
        <h3
          onClick={() => {
            if (readMore === false) {
              handleShow();
            } else if (readMore === true) {
              handleHide();
            }
            setReadMore(!readMore);
          }}
        >
          {linkName}
        </h3>
      </div>
    );
  } else {
    return (
      <div className="text">
        <p id="textInfo">{props.children}</p>
      </div>
    );
  }
}

export default TextLimiter;
